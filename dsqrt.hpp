#ifndef CHIKA_DSQRT_HPP
#define CHIKA_DSQRT_HPP

#include <cinttypes>
#include <optional>

#include "internal_modop.hpp"
#include "prng.hpp"

constexpr std::optional<uint32_t> get_root(uint32_t n, uint32_t P) {
  struct S {
    uint32_t MOD, w;
    uint64_t x, y;
    constexpr S(uint32_t m, uint32_t w_ = -1, uint64_t x_ = 1, uint64_t y_ = 0)
        : MOD(m), w(w_), x(x_), y(y_) {}
    constexpr S operator*(const S &rhs) const {
      return {MOD, rhs.w, (x * rhs.x + y * rhs.y % MOD * rhs.w) % MOD,
              (x * rhs.y + y * rhs.x) % MOD};
    }
  };

  auto resolve = [](S x, int k) {
    S r(x.MOD);
    while (k) {
      if (k & 1)
        r = r * x;
      k >>= 1;
      x = x * x;
    }
    return r.x;
  };

  if (P == 2 or n == 0)
    return n;
  auto check = [&](uint32_t x) { return Chika::mpow(x, (P - 1) / 2, P); };
  if (Chika::mpow(n, (P - 1) / 2, P) != 1)
    return {};
  uint64_t rng[2] = {Chika::PRNG_DEFAULT[0], Chika::PRNG_DEFAULT[1]};
  uint64_t a{};
  uint32_t w{};
  do {
    a = xorshift128plus(rng) % P;
    w = ((a * a + P - n) % P + P) % P;
  } while (check(w) != P - 1);
  return resolve(S(P, w, a, 1), (P + 1) / 2);
}

#endif // CHIKA_DSQRT_HPP
