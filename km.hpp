#ifndef CHIKA_KM_HPP
#define CHIKA_KM_HPP

#include <algorithm>
#include <limits>
#include <vector>

template <typename W = long long> class KM {
private:
  static constexpr W INF = std::numeric_limits<W>::max();
  int n, ql, qr;
  std::vector<W> hl, hr, slk;
  std::vector<int> fl, fr, pre, qu;
  std::vector<std::vector<W>> w;
  std::vector<bool> vl, vr;
  bool check(int x) {
    if (vl[x] = true, fl[x] != -1)
      return vr[qu[qr++] = fl[x]] = true;
    while (x != -1)
      std::swap(x, fr[fl[x] = pre[x]]);
    return false;
  }
  void bfs(int s) {
    std::fill(slk.begin(), slk.end(), INF);
    std::fill(vl.begin(), vl.end(), false);
    std::fill(vr.begin(), vr.end(), false);
    ql = qr = 0;
    vr[qu[qr++] = s] = true;
    while (true) {
      W d;
      while (ql < qr) {
        for (int x = 0, y = qu[ql++]; x < n; ++x) {
          if (!vl[x] && slk[x] >= (d = hl[x] + hr[y] - w[x][y])) {
            if (pre[x] = y, d)
              slk[x] = d;
            else if (!check(x))
              return;
          }
        }
      }
      d = INF;
      for (int x = 0; x < n; ++x)
        if (!vl[x] && d > slk[x])
          d = slk[x];
      for (int x = 0; x < n; ++x) {
        if (vl[x])
          hl[x] += d;
        else
          slk[x] -= d;
        if (vr[x])
          hr[x] -= d;
      }
      for (int x = 0; x < n; ++x)
        if (!vl[x] && !slk[x] && !check(x))
          return;
    }
  }

public:
  KM(int n_)
      : n(n_), hl(n), hr(n), slk(n), fl(n, -1), fr(n, -1), pre(n), qu(n),
        w(n, std::vector<W>(n)), vl(n), vr(n) {}
  void set_edge(int u, int v, W x) { w[u][v] = x; }
  W solve() {
    for (int i = 0; i < n; ++i)
      hl[i] = *max_element(w[i].begin(), w[i].end());
    for (int i = 0; i < n; ++i)
      bfs(i);
    W res = 0;
    for (int i = 0; i < n; ++i)
      res += w[i][fl[i]];
    return res;
  }
  int best_of(int i) const { return fl[i]; }
};
#endif // CHIKA_KM_HPP
