#ifndef CHIKA_MILLER_RABIN_HPP
#define CHIKA_MILLER_RABIN_HPP

#include <cinttypes>
#include <type_traits>

#include "internal_modop.hpp"

template <typename T, std::enable_if_t<std::is_unsigned_v<T>, void *> = nullptr>
constexpr bool isprime(T x) {
  constexpr uint32_t magic[] = {2,      325,     9375,      28178,
                                450775, 9780504, 1795265022};

  auto witn = [](T a, T u, T n, int t) constexpr {
    if (!(a = Chika::mpow(a % n, u, n)))
      return false;
    while (t--) {
      T a2 = Chika::mmul(a, a, n);
      if (a2 == 1 && a != 1 && a != n - 1)
        return true;
      a = a2;
    }
    return a != 1;
  };
  if (x < 2)
    return 0;
  if (!(x & 1))
    return x == 2;
  T x1 = x - 1;
  int t = 0;
  while (!(x1 & 1))
    x1 >>= 1, t++;
  for (uint32_t m : magic)
    if (witn(m, x1, x, t))
      return 0;
  return 1;
}
template <typename T, std::enable_if_t<std::is_signed_v<T>, void *> = nullptr>
constexpr bool isprime(T x) {
  return isprime(std::make_unsigned_t<T>(x));
}

#endif // CHIKA_MILLER_RABIN_HPP
