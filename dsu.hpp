#ifndef CHIKA_DSU_HPP
#define CHIKA_DSU_HPP

#include <algorithm>
#include <cstddef>
#include <numeric>
#include <utility>
#include <vector>

class UndoDSU {
private:
  std::vector<int> fa, sz;
  std::vector<size_t> sv;
  std::vector<std::pair<int *, int>> opt;
  void assign(int *k, int v) {
    opt.emplace_back(k, *k);
    *k = v;
  }

public:
  UndoDSU(int n) : fa(n), sz(n, 1) { std::ranges::iota(fa, 0); }
  int query(int x) const { return fa[x] == x ? x : query(fa[x]); }
  void merge(int a, int b) {
    int af = query(a), bf = query(b);
    if (af == bf)
      return;
    if (sz[af] < sz[bf])
      std::swap(af, bf);
    assign(&fa[bf], fa[af]);
    assign(&sz[af], sz[af] + sz[bf]);
  }
  void save() { sv.push_back(opt.size()); }
  void undo() {
    while (opt.size() > sv.back()) {
      std::pair<int *, int> cur = opt.back();
      *cur.first = cur.second;
      opt.pop_back();
    }
    sv.pop_back();
  }
};

class DSU {
private:
  std::vector<int> fa;

public:
  DSU(int n) : fa(n) { std::iota(fa.begin(), fa.end(), 0); }
  int query(int x) {
    if (fa[x] != x)
      fa[x] = query(fa[x]);
    return fa[x];
  }
  void merge(int a, int b) {
    fa[query(b)] = fa[query(a)];
  }
};

#endif // CHIKA_DSU_HPP
