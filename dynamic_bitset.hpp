#ifndef CHIKA_DYNAMIC_BITSET_HPP
#define CHIKA_DYNAMIC_BITSET_HPP

#include <algorithm>
#include <bitset>
#include <cinttypes>
#include <limits>
#include <vector>

template <size_t B = 256> class DynamicBitset {
  std::size_t n;
  std::vector<std::bitset<B>> a;
  std::bitset<B> lmask;

  std::bitset<B> calc_lmask(std::size_t b) {
    std::bitset<B> ret;
    for (std::size_t i = 0; i <= b; ++i)
      ret[i] = true;
    return ret;
  }

public:
  class reference {
    std::vector<std::bitset<B>> &v;
    std::size_t i, b;

  public:
    reference(std::vector<std::bitset<B>> &v_, std::size_t i_,
              std::size_t b_) noexcept
        : v(v_), i(i_), b(b_) {}
    reference &operator=(bool x) {
      v[i][b] = x;
      return *this;
    }
    operator bool() const noexcept { return v[i][b]; }
    bool operator~() const noexcept { return not bool(*this); }
    reference &flip() noexcept {
      v[i][b].flip();
      return *this;
    }
  };

  explicit DynamicBitset(std::size_t n_) noexcept
      : n(n_), a((n + B - 1) / B), lmask(calc_lmask((n + B - 1) % B)) {}
  explicit DynamicBitset(std::size_t n_, std::uintmax_t x) noexcept
      : DynamicBitset(n_) {
    for (int i = 0; i < std::numeric_limits<std::uintmax_t>::digits; ++i) {
      operator[](i) = (x >> i) & 1;
    }
  }
  void resize(std::size_t n_) {
    n = n_;
    a.resize((n + B - 1) / B);
    lmask = calc_lmask((n + B - 1) % B);
  }
  bool operator[](std::size_t pos) const { return a[pos / B][pos % B]; }
  reference operator[](std::size_t pos) {
    return reference(a, pos / B, pos % B);
  }
  bool all() const noexcept {
    for (std::size_t i = 0; i + 1 < a.size(); ++i) {
      if (not a[i].all()) {
        return false;
      }
    }
    return a.back() == lmask;
  }
  bool any() const noexcept {
    for (const auto &ai : a) {
      if (ai.any()) {
        return true;
      }
    }
    return false;
  }
  bool none() const noexcept { return not any(); }
  std::size_t count() const noexcept {
    std::size_t cnt = 0;
    for (auto ai : a) {
      cnt += ai.count();
    }
    return cnt;
  }
  std::size_t size() const noexcept { return n; }
  DynamicBitset &operator&=(const DynamicBitset &other) noexcept {
    for (std::size_t i = 0; i < a.size(); ++i) {
      if (i == other.size()) {
        break;
      }
      a[i] &= other.a[i];
    }
    return *this;
  }
  DynamicBitset &operator|=(const DynamicBitset &other) noexcept {
    for (std::size_t i = 0; i < a.size(); ++i) {
      if (i == other.size())
        break;
      a[i] |= other.a[i];
    }
    a.back() &= lmask;
    return *this;
  }
  DynamicBitset &operator^=(const DynamicBitset &other) noexcept {
    for (std::size_t i = 0; i < a.size(); ++i) {
      if (i == other.size()) {
        break;
      }
      a[i] ^= other.a[i];
    }
    a.back() &= lmask;
    return *this;
  }
  DynamicBitset operator~() const noexcept {
    DynamicBitset ret(n);
    for (std::size_t i = 0; i < a.size(); ++i) {
      ret.a[i] = ~a[i];
    }
    ret.a.back() &= lmask;
    return ret;
  }
  DynamicBitset operator<<(std::size_t pos) const noexcept {
    DynamicBitset ret(n);
    if (pos >= B) {
      std::size_t offset = pos / B;
      for (std::size_t i = 1; i <= a.size(); ++i) {
        const std::size_t j = a.size() - i;
        if (offset > j) {
          ret.a[j] = 0;
        } else {
          ret.a[j] = a[j - offset];
        }
      }
      pos %= B;
    } else {
      ret.a = a;
    }
    for (std::size_t i = 1; i <= a.size(); ++i) {
      const std::size_t j = a.size() - i;
      if (i != 1) {
        ret.a[j + 1] |= ret.a[j] >> (B - pos);
      }
      ret.a[j] <<= pos;
    }
    ret.a.back() &= lmask;
    return ret;
  }
  DynamicBitset operator<<=(std::size_t pos) noexcept {
    if (pos >= B) {
      std::size_t offset = pos / B;
      for (std::size_t i = 1; i <= a.size(); ++i) {
        const std::size_t j = a.size() - i;
        if (offset > j) {
          a[j] = 0;
        } else {
          a[j] = a[j - offset];
        }
      }
      pos %= B;
    }
    for (std::size_t i = 1; i <= a.size(); ++i) {
      const std::size_t j = a.size() - i;
      if (i != 1) {
        a[j + 1] |= a[j] >> (B - pos);
      }
      a[j] <<= pos;
    }
    a.back() &= lmask;
    return *this;
  }
  DynamicBitset operator>>(std::size_t pos) const noexcept {
    DynamicBitset ret(n);
    ret.a = a;
    if (pos >= B) {
      std::size_t offset = pos / B;
      for (std::size_t i = 0; i < a.size(); ++i) {
        if (i + offset >= a.size()) {
          ret.a[i] = 0;
        } else {
          ret.a[i] = a[i + offset];
        }
      }
      pos %= B;
    } else {
      ret.a = a;
    }
    for (std::size_t i = 0; i < a.size(); ++i) {
      if (i != 0) {
        ret.a[i - 1] |= ret.a[i] << (B - pos);
      }
      ret.a[i] >>= pos;
    }
    ret.a.back() &= lmask;
    return ret;
  }
  DynamicBitset &operator>>=(std::size_t pos) noexcept {
    if (pos >= B) {
      std::size_t offset = pos / B;
      for (std::size_t i = 0; i < a.size(); ++i) {
        if (i + offset >= a.size()) {
          a[i] = 0;
        } else {
          a[i] = a[i + offset];
        }
      }
      pos %= B;
    }
    for (std::size_t i = 0; i < a.size(); ++i) {
      if (i != 0) {
        a[i - 1] |= a[i] << (B - pos);
      }
      a[i] >>= pos;
    }
    a.back() &= lmask;
    return *this;
  }
  DynamicBitset &set() noexcept {
    for (std::size_t i = 0; i < a.size(); ++i) {
      a[i].set();
    }
    a.back() &= lmask;
    return *this;
  }
  DynamicBitset &reset() noexcept {
    for (std::size_t i = 0; i < a.size(); ++i) {
      a[i] = 0;
    }
    return *this;
  }
  DynamicBitset &flip() noexcept {
    for (std::size_t i = 0; i < a.size(); ++i) {
      a[i] = ~a[i];
    }
    a.back() &= lmask;
    return *this;
  }
  template <class CharT = char, class Traits = std::char_traits<CharT>,
            class Allocator = std::allocator<CharT>>
  std::basic_string<CharT, Traits, Allocator>
  to_string(CharT zero = CharT('0'), CharT one = CharT('1')) const {
    std::basic_string<CharT, Traits, Allocator> ret;
    for (std::size_t i = 0; i < a.size(); ++i) {
      auto cur = a[i].template to_string<CharT, Traits, Allocator>(zero, one);
      std::reverse(cur.begin(), cur.end());
      ret += cur;
    }
    while (ret.size() > n) {
      ret.pop_back();
    }
    std::reverse(ret.begin(), ret.end());
    return ret;
  }

  template <class CharT, class Traits>
  friend std::basic_ostream<CharT, Traits> &
  operator<<(std::basic_ostream<CharT, Traits> &os, const DynamicBitset &x) {
    return os << x.to_string();
  }

  friend DynamicBitset operator&(const DynamicBitset &lhs,
                                 const DynamicBitset &rhs) noexcept {
    auto val = lhs;
    val &= rhs;
    return val;
  }
  friend DynamicBitset operator|(const DynamicBitset &lhs,
                                 const DynamicBitset &rhs) noexcept {
    auto val = lhs;
    val |= rhs;
    return val;
  }
  friend DynamicBitset operator^(const DynamicBitset &lhs,
                                 const DynamicBitset &rhs) noexcept {
    auto val = lhs;
    val ^= rhs;
    return val;
  }
};

#endif // CHIKA_DYNAMIC_BITSET_HPP
