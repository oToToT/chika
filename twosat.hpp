#ifndef CHIKA_TWOSAT_HPP
#define CHIKA_TWOSAT_HPP

#include <algorithm>
#include <vector>

class TwoSat {
private:
  int n;
  std::vector<std::vector<int>> G, rG, sccs;
  std::vector<int> ord, idx;
  std::vector<bool> vis, result;
  void dfs(int u) {
    vis[u] = true;
    for (int v : G[u])
      if (!vis[v])
        dfs(v);
    ord.push_back(u);
  }
  void rdfs(int u) {
    vis[u] = false;
    idx[u] = sccs.size() - 1;
    sccs.back().push_back(u);
    for (int v : rG[u])
      if (vis[v])
        rdfs(v);
  }
  void add_edge(int u, int v) {
    G[u].push_back(v);
    rG[v].push_back(u);
  }

public:
  TwoSat(int n_) : n(n_), G(n), rG(n), idx(n), vis(n), result(n) {}

  void Imply(int x, int y) {
    Or(x ^ 1, y);
  }
  void Or(int x, int y) {
    if ((x ^ y) == 1)
      return;
    add_edge(x ^ 1, y);
    add_edge(y ^ 1, x);
  }
  bool Solve() {
    for (int i = 0; i < n; ++i)
      if (not vis[i])
        dfs(i);
    std::reverse(ord.begin(), ord.end());
    for (int u : ord) {
      if (!vis[u])
        continue;
      sccs.push_back(std::vector<int>());
      rdfs(u);
    }
    for (int i = 0; i < n; i += 2)
      if (idx[i] == idx[i + 1])
        return false;
    std::vector<bool> c(sccs.size());
    for (size_t i = 0; i < sccs.size(); ++i) {
      for (int v : sccs[i]) {
        result[v] = c[i];
        c[idx[v ^ 1]] = not c[i];
      }
    }
    return true;
  }
  bool get(int x) const { return result[x]; }
};

#endif // CHIKA_TWOSAT_HPP
