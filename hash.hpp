#ifndef CHIKA_HASH_HPP
#define CHIKA_HASH_HPP

#include "modint.hpp"

#include <ranges>
#include <string_view>
#include <tuple>
#include <utility>
#include <vector>

template <int P = 127, int Q = 1051762951> class Hash {
public:
  using hash_t = modint<Q>;

private:
  std::vector<hash_t> h, p;

public:
  template <typename T>
  Hash(const std::basic_string<T> &s) : Hash(std::basic_string_view<T>(s)) {}
  template <typename T>
  Hash(std::basic_string_view<T> s) : h(s.size() + 1, 0), p(s.size() + 1) {
    for (const auto &[i, si] : s | std::views::enumerate) {
      h[i + 1] = h[i] * P + si;
    }
    generate(p.begin(), p.end(),
             [x = hash_t(1)]() mutable { return std::exchange(x, x * P); });
  }

  hash_t append_shifter(int l) const { return p[l]; }
  // 1-base (l, r]
  hash_t query(int l, int r) const { return h[r] - h[l] * p[r - l]; }
  size_t size() const { return h.size() - 1; }
  template <typename T> void push_back(T c) {
    h.push_back(h.back() * P + c);
    p.push_back(p.back() * P);
  }
};

template <typename H, typename... Hs> class Hashes {
  H h;
  Hashes<Hs...> hs;

public:
  template <typename T> Hashes(const std::basic_string<T> &s) : h(s), hs(s) {}
  auto query(int l, int r) const {
    return std::tuple_cat(std::make_tuple(h.query(l, r)), hs.query(l, r));
  }
  size_t size() const { return h.size(); }
  template <typename T> void push_back(T c) {
    h.push_back(c);
    hs.push_back(c);
  }
};

template <typename H> class Hashes<H> {
  H h;

public:
  template <typename T> Hashes(const std::basic_string<T> &s) : h(s) {}
  auto query(int l, int r) const { return std::make_tuple(h.query(l, r)); }
  size_t size() const { return h.size(); }
  template <typename T> void push_back(T c) { h.push_back(c); }
};

#endif // CHIKA_HASH_HPP
