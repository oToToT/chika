#ifndef CHIKA_PRIMITIVE_ROOT
#define CHIKA_PRIMITIVE_ROOT

#include "factorize.hpp"
#include "internal_modop.hpp"
#include "miller_rabin.hpp"

#include <algorithm>
#include <type_traits>

template <uint64_t P, std::enable_if_t<isprime(P), void *> = nullptr>
struct PrimePrimitiveRoot {
private:
  static constexpr auto P1f = factorize<uint64_t, 64>(P - 1);
  static constexpr bool isPrimitiveRoot(uint64_t x) {
    auto [factors, Size] = P1f;
    for (size_t i = 0; i < Size; ++i) {
      if (Chika::mpow(x, (P - 1) / factors[i], P) == 1)
        return false;
    }
    return true;
  }
  static constexpr uint64_t get() {
    if (P == 2) {
      return 1;
    }
    uint64_t r = 2;
    while (not isPrimitiveRoot(r))
      ++r;
    return r;
  }

public:
  static constexpr uint64_t value = get();
};

uint64_t getPrimitiveRoot(uint64_t P) {
  if (P == 2) {
    return 1;
  }
  auto factors = factorize<uint64_t>(P - 1);
  std::sort(factors.begin(), factors.end());
  factors.erase(std::unique(factors.begin(), factors.end()), factors.end());
  auto isPrimitiveRoot = [P, &factors](uint64_t x) { 
    for (auto f : factors) {
      if (Chika::mpow(x, (P - 1) / f, P) == 1)
        return false;
    }
    return true;
  };
  uint64_t r = 2;
  while (not isPrimitiveRoot(r))
    ++r;
  return r;
}

#endif // CHIKA_PRIMITIVE_ROOT
