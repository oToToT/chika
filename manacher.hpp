#ifndef CHIKA_MANACHER_HPP
#define CHIKA_MANACHER_HPP

#include <algorithm>
#include <string>
#include <vector>

template <typename T>
std::vector<size_t> manacher(const std::basic_string<T> &s, T sep = '.') {
  std::vector<size_t> z(s.size() * 2 + 1);
  std::basic_string<T> t;
  t += sep;
  for (auto c : s)
    t += c, t += sep;
  size_t l = 0, r = 0;
  for (size_t i = 1; i < t.size(); ++i) {
    z[i] = (r > i ? std::min(z[2 * l - i], r - i) : 1);
    while (i >= z[i] and i + z[i] < t.size()) {
      if (t[i - z[i]] == t[i + z[i]])
        ++z[i];
      else
        break;
    }
    if (i + z[i] > r)
      r = i + z[i], l = i;
  }
  return z;
}

#endif // CHIKA_MANACHER_HPP
