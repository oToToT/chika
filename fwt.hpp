#ifndef CHIKA_FWT_HPP
#define CHIKA_FWT_HPP

#include "modint.hpp"

template <typename T> constexpr void fwt_xor(T x[], int N) {
  for (int d = 1; d < N; d <<= 1) {
    int d2 = d << 1;
    for (int s = 0; s < N; s += d2)
      for (int i = s, j = s + d; i < s + d; i++, j++) {
        auto ta = x[i], tb = x[j];
        x[i] = ta + tb;
        x[j] = ta - tb;
      }
  }
}
template <typename T> constexpr void ifwt_xor(T x[], int N) {
  fwt_xor(x, N);
  for (int i = 0; i < N; i++) {
    x[i] /= N;
  }
}
template <unsigned int MOD> constexpr void ifwt_xor(modint<MOD> x[], int N) {
  fwt_xor(x, N);
  const auto invn = modint<MOD>(N).inv();
  for (int i = 0; i < N; i++) {
    x[i] *= invn;
  }
}
template <typename T> constexpr void fwt_or(T x[], int N) {
  for (int d = 1; d < N; d <<= 1) {
    int d2 = d << 1;
    for (int s = 0; s < N; s += d2)
      for (int i = s, j = s + d; i < s + d; i++, j++) {
        auto ta = x[i], tb = x[j];
        x[i] = ta;
        x[j] = ta + tb;
      }
  }
}
template <typename T> constexpr void ifwt_or(T x[], int N) {
  for (int d = 1; d < N; d <<= 1) {
    int d2 = d << 1;
    for (int s = 0; s < N; s += d2)
      for (int i = s, j = s + d; i < s + d; i++, j++) {
        auto ta = x[i], tb = x[j];
        x[i] = ta;
        x[j] = tb - ta;
      }
  }
}
template <typename T> constexpr void fwt_and(T x[], int N) {
  for (int d = 1; d < N; d <<= 1) {
    int d2 = d << 1;
    for (int s = 0; s < N; s += d2)
      for (int i = s, j = s + d; i < s + d; i++, j++) {
        auto ta = x[i], tb = x[j];
        x[i] = ta + tb;
        x[j] = tb;
      }
  }
}
template <typename T> constexpr void ifwt_and(T x[], int N) {
  for (int d = 1; d < N; d <<= 1) {
    int d2 = d << 1;
    for (int s = 0; s < N; s += d2)
      for (int i = s, j = s + d; i < s + d; i++, j++) {
        auto ta = x[i], tb = x[j];
        x[i] = ta - tb;
        x[j] = tb;
      }
  }
}
#endif // CHIKA_FWT_HPP
