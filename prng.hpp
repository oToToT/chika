#ifndef CHIKA_PRNG_HPP
#define CHIKA_PRNG_HPP

#include <cinttypes>
#include <limits>
#include <utility>

constexpr uint64_t xorshift128plus(uint64_t s[2]) {
  uint64_t s1 = s[0];
  uint64_t s0 = s[1];
  uint64_t result = s0 + s1;
  s[0] = s0;
  s1 ^= s1 << 23;
  s[1] = s1 ^ s0 ^ (s1 >> 17) ^ (s0 >> 26);
  return result;
}

namespace Chika {
static constexpr uint64_t PRNG_DEFAULT[2] = {16001192987876798580ULL,
                                             7043191799336571417ULL};
};

#endif // CHIKA_PRNG_HPP
