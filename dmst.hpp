#ifndef CHIKA_DMST_HPP
#define CHIKA_DMST_HPP

#include <cstddef>
#include <numeric>
#include <queue>
#include <tuple>
#include <utility>

// vertex index lies in 0-base [0, n)
// TODO: optimize this with mergeable heap
template <typename T>
std::vector<int> dmst(const std::vector<std::tuple<int, int, T>> &edges, int n,
                      int root) {
  using PQ = std::pair<
      std::priority_queue<std::pair<T, int>, std::vector<std::pair<T, int>>,
                          std::greater<std::pair<T, int>>>,
      T>;
  auto push = [](PQ &pq, std::pair<T, int> v) {
    pq.first.emplace(v.first - pq.second, v.second);
  };
  auto top = [](const PQ &pq) -> std::pair<T, int> {
    auto r = pq.first.top();
    return {r.first + pq.second, r.second};
  };
  auto join = [&push, &top](PQ &pq1, PQ &pq2) {
    if (pq1.first.size() < pq2.first.size()) {
      swap(pq1, pq2);
    }
    while (not pq2.first.empty()) {
      push(pq1, top(pq2));
      pq2.first.pop();
    }
  };

  std::vector<PQ> v(n * 2);
  for (size_t i = 0; i < edges.size(); ++i) {
    push(v[std::get<1>(edges[i])], {std::get<2>(edges[i]), i});
  }
  std::vector<int> dsu(n * 2);
  std::iota(dsu.begin(), dsu.end(), 0);
  auto dsu_query = [&dsu](int x) {
    int y;
    for (y = x; dsu[y] != y; y = dsu[y])
      ;
    for (int ox = x; x != y; ox = x)
      x = dsu[x], dsu[ox] = y;
    return y;
  };
  std::vector<int> vis(n * 2, -1), pa(n * 2, -1), res(n * 2);
  vis[root] = n + 1;
  int pc = n;
  for (int i = 0; i < n; ++i)
    if (vis[i] == -1) {
      for (int p = i; vis[p] == -1 or vis[p] == i;
           p = dsu_query(std::get<0>(edges[res[p]]))) {
        if (vis[p] == i) {
          int q = p;
          p = pc++;
          do {
            v[q].second = -v[q].first.top().first;
            join(v[pa[q] = dsu[q] = p], v[q]);
            q = dsu_query(std::get<0>(edges[res[q]]));
          } while (q != p);
        }
        vis[p] = i;
        while (not v[p].first.empty() and
               dsu_query(std::get<0>(edges[top(v[p]).second])) == p) {
          v[p].first.pop();
        }
        res[p] = top(v[p]).second;
      }
    }

  std::vector<int> ans;
  for (int i = pc - 1; i >= 0; i--)
    if (i != root and vis[i] != n) {
      for (int f = std::get<1>(edges[res[i]]); f != -1 and vis[f] != n;
           f = pa[f])
        vis[f] = n;
      ans.push_back(res[i]);
    }
  return ans;
}

#endif // CHIKA_DMST_HPP
