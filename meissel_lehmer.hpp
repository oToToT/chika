#ifndef CHIKA_MEISSEL_LEHMER_HPP
#define CHIKA_MEISSEL_LEHMER_HPP

#include <algorithm>
#include <cstddef>
#include <vector>

class MeisselLehmer {
  static constexpr size_t MAX_GAP = 150;
  static constexpr size_t cube_root(size_t x) {
    size_t L = 0, R = x + 1;
    while (R - L > 1) {
      size_t M = (L + R) >> 1;
      if (M * M * M <= x)
        L = M;
      else
        R = M;
    }
    return L;
  }
  static constexpr size_t square_root(size_t x) {
    size_t L = 0, R = x + 1;
    while (R - L > 1) {
      size_t M = (L + R) >> 1;
      if (M * M <= x)
        L = M;
      else
        R = M;
    }
    return L;
  }

  std::vector<size_t> pi, primes;
  std::vector<std::vector<size_t>> phi_table;

  size_t P2(size_t m, size_t n) {
    auto &self = *this;
    size_t ret = 0;
    for (size_t i = n + 1; primes[i] * primes[i] <= m; i++)
      ret += self(m / primes[i]) - self(primes[i]) + 1;
    return ret;
  }

  size_t phi(size_t m, size_t n) {
    if (n == 0)
      return m;
    if (primes[n] >= m)
      return 1;

    if (m >= phi_table.size() or n >= phi_table[m].size()) {
      return phi(m, n - 1) - phi(m / primes[n], n - 1);
    }
    if (phi_table[m][n] == 0) {
      phi_table[m][n] = phi(m, n - 1) - phi(m / primes[n], n - 1) + 1;
    }
    return phi_table[m][n] - 1;
  }

public:
  explicit MeisselLehmer(size_t N)
      : pi(square_root(N) + MAX_GAP), primes({1}),
        phi_table(cube_root(N) + 100, std::vector<size_t>(cube_root(N) + 100)) {
    const size_t m = pi.size();
    std::vector<bool> sieved(m);
    primes.reserve(m);
    for (size_t i = 2; i < m; ++i) {
      if (not sieved[i])
        primes.push_back(i);
      pi[i] = pi[i - 1] + (not sieved[i]);
      for (auto p : primes) {
        if (p == 1)
          continue;
        if (p * i >= m)
          break;
        sieved[p * i] = true;
        if (p % i == 0)
          break;
      }
    }
  }
  size_t operator()(size_t n) {
    if (n < pi.size())
      return pi[n];
    auto &self = *this;
    size_t m = self(cube_root(n));
    return phi(n, m) + m - 1 - P2(n, m);
  }
};

#endif // CHIKA_MEISSEL_LEHMER_HPP
