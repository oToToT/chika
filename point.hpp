#ifndef CHIKA_POINT_HPP
#define CHIKA_POINT_HPP

#include "eps.hpp"

#include <cmath>
#include <type_traits>

template <typename T = int> struct Pt {
  T x, y;

  constexpr Pt(T x_ = 0, T y_ = 0) : x(x_), y(y_) {}

  constexpr int argCmp(const Pt &that) const {
    // -1 / 0 / 1 <-> < / == / > (atan2)
    int qa = (eq(y, 0) ? (lt(x, 0) ? 3 : 1) : (lt(y, 0) ? 0 : 2));
    int qb =
        (eq(that.y, 0) ? (lt(that.x, 0) ? 3 : 1) : (lt(that.y, 0) ? 0 : 2));
    if (qa != qb)
      return sgn(qa - qb);
    return sgn(that ^ *this);
  }
  constexpr Pt rot90() const { return Pt(-y, x); }
  constexpr Pt operator+(const Pt &rhs) const { return Pt(x + rhs.x, y + rhs.y); }
  constexpr Pt operator-(const Pt &rhs) const { return Pt(x - rhs.x, y - rhs.y); }
  constexpr Pt operator*(T v) const { return P(x * v, y * v); }
  constexpr T operator*(const Pt &that) const { return x * that.x + y * that.y; }
  constexpr T operator^(const Pt &that) const { return x * that.y - y * that.x; }
  constexpr bool operator==(const Pt &that) const {
    return eq(x, that.x) and eq(y, that.y);
  }
  constexpr bool operator<(const Pt &that) const {
    return (not eq(x, that.x)) ? lt(x, that.x) : lt(y, that.y);
  }

private:
  static constexpr bool lt(T a, T b) {
    if constexpr (std::is_floating_point_v<T>) {
      return a + EPS < b;
    } else {
      return a < b;
    }
  }
  static constexpr bool eq(T a, T b) {
    if constexpr (std::is_floating_point_v<T>) {
      return std::abs(a - b) < EPS;
    } else {
      return a == b;
    }
  }
  static constexpr int sgn(T a) { return eq(a, 0) ? 0 : (lt(0, a) - lt(a, 0)); }
};

template <typename T> constexpr Pt<T> operator*(T v, const Pt<T> &a) { return a * v; }

#endif // CHIKA_POINT_HPP
