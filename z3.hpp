#ifndef CHIKA_Z3_HPP
#define CHIKA_Z3_HPP

#include <algorithm>
#include <cinttypes>
#include <vector>

template <typename T = int64_t> class Z3 {
  std::vector<T> v;

public:
  Z3() : v() {}
  void insert(const T &x) { v.push_back(x); }
  void insert(T &&x) { v.emplace_back(std::move(x)); }
  void done() {
    std::sort(v.begin(), v.end());
    v.erase(std::unique(v.begin(), v.end()), v.end());
  }
  ssize_t ssize() const { return ssize_t(v.size()); }
  size_t size() const { return v.size(); }
  auto get(const T &x) const {
    return std::distance(v.begin(), std::lower_bound(v.begin(), v.end(), x));
  }
  T iget(size_t i) const { return v[i]; }
  void clear() { v.clear(); }
};

#endif // CHIKA_Z3_HPP
