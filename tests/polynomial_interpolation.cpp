#include "polynomial.hpp"

#include <iostream>

using P = Poly<998244353>;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  P::ctnr_t x(n), y(n);
  for (int i = 0; i < n; ++i)
    std::cin >> x[i];
  for (int i = 0; i < n; ++i)
    std::cin >> y[i];

  P ans = P::Interpolate(x, y);
  for (int i = 0; i < n; ++i)
    std::cout << ans[i] << " \n"[i + 1 == n];
  return 0;
}
