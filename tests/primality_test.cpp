#include "miller_rabin.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int q;
  std::cin >> q;
  while (q--) {
    uint64_t n;
    std::cin >> n;
    std::cout << (isprime(n) ? "Yes" : "No") << "\n";
  }
  return 0;
}
