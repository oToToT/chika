#include "meissel_lehmer.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  size_t n;
  std::cin >> n;
  std::cout << MeisselLehmer(n)(n) << '\n';
  return 0;
}
