#include "shortest_path.hpp"

#include <algorithm>
#include <cinttypes>
#include <iostream>
#include <limits>
#include <utility>
#include <vector>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, m, s, t;
  std::cin >> n >> m >> s >> t;
  std::vector<std::vector<std::pair<int, int64_t>>> g(n), rg(n);
  while (m--) {
    int u, v, w;
    std::cin >> u >> v >> w;
    g[u].emplace_back(v, w);
    rg[v].emplace_back(u, w);
  }
  auto [dist, from] = shortest_path<int64_t, true>(g, s);
  if (dist[t] == std::numeric_limits<int64_t>::max()) {
    std::cout << "-1\n";
    return 0;
  }
  std::vector<int> path = {t};
  while (path.back() != s) {
    path.push_back(from[path.back()]);
  }
  std::reverse(path.begin(), path.end());
  std::cout << dist[t] << ' ' << path.size() - 1 << '\n';
  for (size_t i = 1; i < path.size(); ++i)
    std::cout << path[i - 1] << ' ' << path[i] << '\n';
  return 0;
}
