#include "sparse_table.hpp"

#include <iostream>
#include <vector>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, q;
  std::cin >> n >> q;
  std::vector<int> a(n);
  for (int &ai : a)
    std::cin >> ai;

  SparseTable<int> table(a.data(), n);
  while (q--) {
    int l, r;
    std::cin >> l >> r;
    std::cout << table.query(l, r) << '\n';
  }
  return 0;
}
