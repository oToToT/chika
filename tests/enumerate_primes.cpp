#include "primes.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, a, b;
  std::cin >> n >> a >> b;
  Primes ps(n + 1);
  const int m = ps.count();
  std::vector<int> ans;
  for (int i = 0; a * i + b < m; ++i)
    ans.push_back(ps[a * i + b]);
  std::cout << m << ' ' << ans.size() << '\n';
  for (size_t i = 0; i < ans.size(); ++i)
    std::cout << ans[i] << " \n"[i + 1 == ans.size()];
  return 0;
}
