#include "lct.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  struct V {
    int64_t v;
    V() : v(0) {}
    explicit V(int64_t v_) : v(v_) {}
    V operator*(const V &rhs) const {
      return V(v + rhs.v);
    }
  };
  int n, q;
  std::cin >> n >> q;
  std::vector<int64_t> a(n);
  LCT<V> lct(n);
  for (int i = 0; i < n; ++i) {
    std::cin >> a[i];
    lct.set_val(i, V(a[i]));
  }
  for (int i = 1; i < n; ++i) {
    int u, v;
    std::cin >> u >> v;
    lct.add_edge(u, v);
  }
  while (q--) {
    int op;
    std::cin >> op;
    if (op == 0) {
      int u, v, c, d;
      std::cin >> u >> v >> c >> d;
      lct.del_edge(u, v);
      lct.add_edge(c, d);
    } else if (op == 1) {
      int p; int64_t x;
      std::cin >> p >> x;
      a[p] += x;
      lct.set_val(p, V(a[p]));
    } else {
      int u, v;
      std::cin >> u >> v;
      std::cout << lct.query(u, v).v << '\n';
    }
  }
  return 0;
}
