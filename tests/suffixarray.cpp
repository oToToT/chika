#include "sais.hpp"

#include <iostream>

SAIS<500000 + 5, int64_t> sais;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  std::string s_;
  std::cin >> s_;
  std::basic_string<int64_t> s;
  for (auto c : s_)
    s += c;
  sais.build(s);
  for (size_t i = 0; i < s.size(); ++i)
    std::cout << sais.sa[i] << " \n"[i + 1 == s.size()];
  return 0;
}
