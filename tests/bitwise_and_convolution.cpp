#include "fwt.hpp"
#include "modint.hpp"

#include <iostream>
#include <vector>

using mint = modint<998244353>;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  n = 1 << n;
  std::vector<mint> a(n), b(n);
  for (auto &ai : a)
    std::cin >> ai;
  for (auto &bi : b)
    std::cin >> bi;
  fwt_and(a.data(), n);
  fwt_and(b.data(), n);
  for (int i = 0; i < n; ++i)
    a[i] *= b[i];
  ifwt_and(a.data(), n);
  for (int i = 0; i < n; ++i)
    std::cout << a[i] << " \n"[i + 1 == n];
  return 0;
}
