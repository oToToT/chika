#include "primitive_root.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int t;
  std::cin >> t;
  while (t--) {
    uint64_t x;
    std::cin >> x;
    std::cout << getPrimitiveRoot(x) << '\n';
  }
  return 0;
}
