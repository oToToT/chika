#include "modint.hpp"
#include "ntt.hpp"

#include <iostream>
#include <vector>

using mint = modint<998244353>;
NTT<998244353> ntt;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  size_t n, m;
  std::cin >> n >> m;
  std::vector<mint> a(n), b(m);
  for (auto &ai : a)
    std::cin >> ai;
  for (auto &bi : b)
    std::cin >> bi;

  size_t k = 1;
  while (k < n + m)
    k *= 2;

  a.resize(k);
  b.resize(k);

  ntt(a.data(), k);
  ntt(b.data(), k);
  for (size_t i = 0; i < k; ++i)
    a[i] *= b[i];
  ntt(a.data(), k, true);
  for (size_t i = 1; i < n + m; ++i)
    std::cout << a[i - 1] << " \n"[i == n + m - 1];
  return 0;
}
