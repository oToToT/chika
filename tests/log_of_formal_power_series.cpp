#include "polynomial.hpp"

#include <iostream>

using P = Poly<998244353>;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  P a(n);
  for (int i = 0; i < n; ++i)
    std::cin >> a[i];
  auto ans = a.Ln();
  for (int i = 0; i < n; ++i)
    std::cout << ans[i] << " \n"[i + 1 == n];
  return 0;
}
