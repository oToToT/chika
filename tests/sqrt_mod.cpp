#include "dsqrt.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int t;
  std::cin >> t;
  while (t--) {
    int x, p;
    std::cin >> x >> p;
    auto r = get_root(x, p);
    if (not r)
      std::cout << "-1\n";
    else
      std::cout << *r << '\n';
  }
  return 0;
}
