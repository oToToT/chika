#include "polynomial.hpp"

#include <iostream>
#include <vector>

using P = Poly<998244353>;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  std::vector<P> ps(n);
  for (auto &pi : ps) {
    int d;
    std::cin >> d;
    pi.resize(d + 1);
    for (auto &pij : pi)
      std::cin >> pij;
  }
  ps.emplace_back(P::ctnr_t{1});

  auto dc = [&](auto self, size_t l, size_t r) -> void {
    if (r - l == 1) {
      return;
    }
    auto m = (l + r) >> 1;
    self(self, l, m);
    self(self, m, r);
    ps[l] = ps[l].Mul(ps[m]);
  };
  dc(dc, 0, ps.size());
  for (size_t i = 0; i < ps[0].size(); ++i)
    std::cout << ps[0][i] << " \n"[i + 1 == ps[0].size()];
  return 0;
}
