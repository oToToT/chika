#include "manacher.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  std::string s; std::cin >> s;
  auto z = manacher(s);
  for (size_t i = 1; i + 1 < z.size(); ++i)
    std::cout << z[i] - 1 << " \n"[i + 2 == z.size()];
  return 0;
}
