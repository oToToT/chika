#include <iostream>
#include <vector>

#include "lbd.hpp"

class BIT {
  static int lowbit(int x) { return x & (-x); }
  int n;
  std::vector<int64_t> v;

  int64_t query(int p) const {
    int64_t r = 0;
    while (p) {
      r += v[p];
      p -= lowbit(p);
    }
    return r;
  }

public:
  BIT(int n_) : n(n_), v(n) {}
  void add(int p, int64_t x) {
    while (p < n) {
      v[p] += x;
      p += lowbit(p);
    }
  }
  int64_t query(int l, int r) const { return query(r) - query(l); }
};

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, q;
  std::cin >> n >> q;
  std::vector<int> a(n);
  for (auto &ai : a)
    std::cin >> ai;
  LBD tree(n);
  for (int i = 1; i < n; ++i) {
    int u, v;
    std::cin >> u >> v;
    tree.add_edge(u, v);
  }
  tree.decompose();
  BIT bit(n + 1);
  for (int i = 0; i < n; ++i) {
    int p = tree.get_path(i, i)[0].second;
    bit.add(p, a[i]);
  }
  while (q--) {
    int op;
    std::cin >> op;
    if (op == 0) {
      int i, x;
      std::cin >> i >> x;
      int p = tree.get_path(i, i)[0].second;
      bit.add(p, x);
    } else {
      int u, v;
      std::cin >> u >> v;
      int64_t ans = 0;
      for (auto [tl, tr] : tree.get_path(u, v))
        ans += bit.query(tl, tr);
      std::cout << ans << '\n';
    }
  }
  return 0;
}
