#include "z3.hpp"

#include <algorithm>
#include <iostream>
#include <vector>

class PSegTree {
  struct node {
    int sm;
    int lc, rc;
    node() : sm(0), lc(0), rc(0) {}
  };
  std::vector<node> nodes;
  int alloc() {
    int r = int(nodes.size());
    nodes.push_back(node());
    return r;
  }

  int n;
  std::vector<int> roots;
  int add(int ql, int qr, int l, int r, int id, int v) {
    if (qr <= l or r <= ql)
      return id;
    int nid = alloc();
    nodes[nid] = nodes[id];
    if (ql <= l and r <= qr) {
      nodes[nid].sm += v;
      return nid;
    }
    int m = (l + r) >> 1;
    nodes[nid].lc = add(ql, qr, l, m, nodes[id].lc, v);
    nodes[nid].rc = add(ql, qr, m, r, nodes[id].rc, v);
    nodes[nid].sm = nodes[nodes[nid].lc].sm + nodes[nodes[nid].rc].sm;
    return nid;
  }
  int query(int l, int r, int lid, int rid, int k) {
    if (r - l == 1)
      return l;
    int m = (l + r) >> 1;
    int lv = nodes[nodes[rid].lc].sm - nodes[nodes[lid].lc].sm;
    if (k - lv >= 0)
      return query(m, r, nodes[lid].rc, nodes[rid].rc, k - lv);
    return query(l, m, nodes[lid].lc, nodes[rid].lc, k);
  }

public:
  PSegTree(int n_) : nodes(1), n(n_), roots() { roots.push_back(alloc()); }
  void add(int p, int v) {
    roots.push_back(add(p, p + 1, 0, n, roots.back(), v));
  }
  int query(int l, int r, int k) { return query(0, n, roots[l], roots[r], k); }
};

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, q;
  std::cin >> n >> q;
  std::vector<int> a(n);
  for (int &ai : a)
    std::cin >> ai;

  Z3 z;
  for (int &ai : a)
    z.insert(ai);
  z.done();
  PSegTree pst(z.ssize());
  for (int ai : a)
    pst.add(z.get(ai), 1);

  for (int i = 0; i < q; ++i) {
    int l, r, k;
    std::cin >> l >> r >> k;
    std::cout << z.iget(pst.query(l, r, k)) << '\n';
  }
  return 0;
}
