#include "point.hpp"

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  using P = Pt<int64_t>;
  std::vector<P> ps(n);
  for (auto &pi : ps)
    std::cin >> pi.x >> pi.y;
  std::sort(ps.begin(), ps.end(),
            [](const auto &p0, const auto &p1) { return p0.argCmp(p1) < 0; });
  for (const auto &pi : ps)
    std::cout << pi.x << ' ' << pi.y << '\n';
  return 0;
}
