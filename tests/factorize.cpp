#include "factorize.hpp"

#include <algorithm>
#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int t;
  std::cin >> t;
  while (t--) {
    uint64_t x;
    std::cin >> x;
    auto f = factorize(x);
    std::sort(f.begin(), f.end());
    std::cout << f.size();
    for (auto fi : f)
      std::cout << ' ' << fi;
    std::cout << '\n';
  }
  return 0;
}
