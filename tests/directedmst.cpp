#include "dmst.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, m, s;
  std::cin >> n >> m >> s;
  std::vector<std::tuple<int, int, int64_t>> e(m);
  for (auto &[u, v, w] : e)
    std::cin >> u >> v >> w;
  auto eids = dmst(e, n, s);
  std::vector<size_t> ans(n, s);
  int64_t tot = 0;
  for (auto eid : eids) {
    tot += std::get<2>(e[eid]);
    ans[std::get<1>(e[eid])] = std::get<0>(e[eid]);
  }
  std::cout << tot << '\n';
  for (int i = 0; i < n; ++i)
    std::cout << ans[i] << " \n"[i + 1 == n];
  return 0;
}
