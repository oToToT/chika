#include "polynomial.hpp"

#include <iostream>

using P = Poly<998244353>;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, m;
  std::cin >> n >> m;
  P f(n);
  P::ctnr_t x(m);
  for (int i = 0; i < n; ++i)
    std::cin >> f[i];
  for (int i = 0; i < m; ++i)
    std::cin >> x[i];

  auto ans = f.Eval(x);
  for (int i = 0; i < m; ++i)
    std::cout << ans[i] << " \n"[i + 1 == m];
  return 0;
}
