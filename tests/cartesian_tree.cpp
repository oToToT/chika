#include "cartesian_tree.hpp"

#include <algorithm>
#include <iostream>
#include <print>
#include <ranges>
#include <vector>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  std::vector<int> a(n);
  std::ranges::for_each(a, [](auto &ai) { std::cin >> ai; });
  for (auto [index, parent] : get_cartesian_tree(a) | std::views::enumerate) {
    if (index) {
      std::print(" ");
    }
    std::print("{}", parent);
  }
  std::println("");
  return 0;
}
