#include "lct.hpp"
#include "modint.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);

  using mint = modint<998244353>;

  struct F {
    mint a, b;
    F() : a{1}, b{0} {}
    F(mint c0, mint c1) : a{c0}, b{c1} {}
    F operator*(const F &rhs) const {
      return F(a * rhs.a, b * rhs.a + rhs.b);
    }
    mint operator()(mint x) const { return a * x + b; }
  };

  int n, q;
  std::cin >> n >> q;
  LCT<F> lct(n);
  for (int i = 0; i < n; ++i) {
    int x, y;
    std::cin >> x >> y;
    lct.set_val(i, F{x, y});
  }
  for (int i = 1; i < n; ++i) {
    int u, v;
    std::cin >> u >> v;
    lct.add_edge(u, v);
  }
  while (q--) {
    int op;
    std::cin >> op;
    if (op == 0) {
      int a, b, c, d;
      std::cin >> a >> b >> c >> d;
      lct.del_edge(a, b);
      lct.add_edge(c, d);
    } else if (op == 1) {
      int p, c, d;
      std::cin >> p >> c >> d;
      lct.set_val(p, F{c, d});
    } else {
      int u, v, x;
      std::cin >> u >> v >> x;
      std::cout << lct.query(u, v)(x) << '\n';
    }
  }
  return 0;
}
