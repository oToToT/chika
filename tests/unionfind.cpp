#include "dsu.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, q;
  std::cin >> n >> q;
  DSU dsu(n);
  while (q--) {
    int op, u, v;
    std::cin >> op >> u >> v;
    if (op) {
      std::cout << (dsu.query(u) == dsu.query(v)) << '\n';
    } else {
      dsu.merge(u, v);
    }
  }
  return 0;
}
