#include "dinic.hpp"

#include <iostream>
#include <utility>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int l, r, m;
  std::cin >> l >> r >> m;
  std::vector<std::pair<int, int>> e(m);
  for (auto &[a, b] : e)
    std::cin >> a >> b;

  Dinic flow(l + r + 2);
  const int S = l + r, T = l + r + 1;
  for (auto [a, b] : e)
    flow.add_edge(a, b + l, 1);
  for (int i = 0; i < l; ++i)
    flow.add_edge(S, i, 1);
  for (int i = 0; i < r; ++i)
    flow.add_edge(i + l, T, 1);

  std::cout << flow(S, T) << '\n';
  for (int i = 0; i < m; ++i) {
    if (flow[i] == 1) {
      std::cout << e[i].first << ' ' << e[i].second << '\n';
    }
  }
  return 0;
}
