#include "scc.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, m;
  std::cin >> n >> m;
  SCC scc(n);
  for (int i = 0; i < m; ++i) {
    int u, v;
    std::cin >> u >> v;
    scc.add_edge(u, v);
  }
  scc.solve();
  std::vector<std::vector<int>> sccs(scc.count());
  for (int i = 0; i < n; ++i)
    sccs[scc.get(i)].push_back(i);
  std::cout << scc.count() << '\n';
  for (int i = 0; i < scc.count(); ++i) {
    std::cout << sccs[i].size();
    for (int x : sccs[i])
      std::cout << ' ' << x;
    std::cout << '\n';
  }
  return 0;
}
