#include "bcc.hpp"

#include <iostream>
#include <queue>
#include <unordered_set>
#include <utility>
#include <vector>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, m;
  std::cin >> n >> m;
  BCC bcc(n);
  std::vector<std::pair<int, int>> e(m);
  for (auto &[u, v] : e) {
    std::cin >> u >> v;
    bcc.add_edge(u, v);
  }
  bcc.solve();
  std::unordered_set<int> s;
  for (int i = 0; i < n; ++i)
    s.insert(i);
  std::vector<std::unordered_set<int>> a(m);
  for (int i = 0; i < m; ++i) {
    auto [u, v] = e[i];
    int o = bcc.bcc_id(i);
    a[o].insert(u);
    a[o].insert(v);
    s.erase(u);
    s.erase(v);
  }
  std::vector<std::vector<int>> ans;
  for (const auto &si : a) {
    if (si.empty())
      continue;
    ans.emplace_back(si.begin(), si.end());
  }
  for (auto x : s) {
    ans.push_back({x});
  }
  std::cout << ans.size() << '\n';
  for (const auto &v : ans) {
    std::cout << v.size();
    for (auto vi : v)
      std::cout << ' ' << vi;
    std::cout << '\n';
  }
  return 0;
}
