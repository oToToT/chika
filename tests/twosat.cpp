#include "twosat.hpp"

#include <iostream>
#include <string>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  std::string _;
  std::cin >> _ >> _;
  int n, q;
  std::cin >> n >> q;
  TwoSat solver(n * 2);
  auto normalize = [](int x) {
    x *= 2;
    if (x < 0)
      return -x - 1;
    return x - 2;
  };
  while (q--) {
    int x, y;
    std::cin >> x >> y >> _;
    x = normalize(x);
    y = normalize(y);
    solver.Or(x, y);
  }
  if (solver.Solve()) {
    std::cout << "s SATISFIABLE\nv";
    for (int i = 0; i < n; ++i) {
      if (solver.get(i * 2)) {
        std::cout << ' ' << i + 1;
      } else {
        std::cout << ' ' << -i - 1;
      }
    }
    std::cout << " 0\n";
  } else {
    std::cout << "s UNSATISFIABLE\n";
  }
  return 0;
}
