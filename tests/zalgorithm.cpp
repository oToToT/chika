#include "zalgorithm.hpp"

#include <iostream>
#include <string>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  std::string s;
  std::cin >> s;
  auto z = zalgo(s);
  for (size_t i = 0; i < s.size(); ++i)
    std::cout << z[i] << " \n"[i + 1 == s.size()];
  return 0;
}
