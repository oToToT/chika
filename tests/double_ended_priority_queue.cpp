#include "minmax_heap.hpp"

#include "io.hpp"

int main() {
  MinMaxHeap<int> pq;
  int n, q;
  fin >> n >> q;
  while (n--) {
    int x;
    fin >> x;
    pq.push(x);
  }
  while (q--) {
    int op;
    fin >> op;
    if (op == 0) {
      int x;
      fin >> x;
      pq.emplace(x);
    } else if (op == 1) {
      fout << pq.getMin() << '\n';
      pq.popMin();
    } else {
      fout << pq.getMax() << '\n';
      pq.popMax();
    }
  }
  return 0;
}
