#include "polynomial.hpp"

#include <iostream>

using P = Poly<998244353>;

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n, m;
  std::cin >> n >> m;
  P f(n), g(m);
  for (int i = 0; i < n; ++i)
    std::cin >> f[i];
  for (int i = 0; i < m; ++i)
    std::cin >> g[i];

  auto [q, r] = f.DivMod(g);
  std::cout << q.size() << ' ' << r.size() << '\n';
  for (size_t i = 0; i < q.size(); ++i)
    std::cout << q[i] << " \n"[i + 1 == q.size()];
  if (q.empty()) std::cout << '\n';
  for (size_t i = 0; i < r.size(); ++i)
    std::cout << r[i] << " \n"[i + 1 == r.size()];
  if (r.empty()) std::cout << '\n';
  return 0;
}
