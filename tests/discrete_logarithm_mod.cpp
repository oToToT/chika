#include "dlog.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int t;
  std::cin >> t;
  while (t--) {
    int x, y, m;
    std::cin >> x >> y >> m;
    if (auto l = dlog<int64_t>(x, y, m))
      std::cout << *l << '\n';
    else
      std::cout << -1 << '\n';
  }
  return 0;
}
