#include "gcd_convolution.hpp"

#include <iostream>
#include <vector>

#include "modint.hpp"

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  using mint = modint<998244353>;
  int n;
  std::cin >> n;
  std::vector<mint> a(n + 1), b(n + 1);
  for (int i = 1; i <= n; ++i)
      std::cin >> a[i];
  for (int i = 1; i <= n; ++i)
      std::cin >> b[i];
  auto c = gcd_convolution(a, b);
  for (int i = 1; i <= n; ++i)
    std::cout << c[i] << " \n"[i == n];
  return 0;
}
