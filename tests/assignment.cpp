#include "km.hpp"

#include <iostream>

int main() {
  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  KM km(n);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      int x;
      std::cin >> x;
      km.set_edge(i, j, -x);
    }
  }
  std::cout << -km.solve() << '\n';
  for (int i = 0; i < n; ++i)
    std::cout << km.best_of(i) << " \n"[i + 1 == n];
  return 0;
}
