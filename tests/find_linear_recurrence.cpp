#include "berlekamp_massey.hpp"
#include "modint.hpp"

#include <iostream>
#include <vector>


int main() {
  using mint = modint<998244353>;

  std::cin.tie(nullptr)->sync_with_stdio(false);
  int n;
  std::cin >> n;
  std::vector<mint> a(n);
  for (auto &ai : a)
    std::cin >> ai;

  auto b = BerlekampMassey(a);
  std::cout << b.size() << '\n';
  for (size_t i = 0; i < b.size(); ++i)
    std::cout << b[i] << " \n"[i + 1 == b.size()];
  return 0;
}
