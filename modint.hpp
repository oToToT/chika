#ifndef CHIKA_MODINT_HPP
#define CHIKA_MODINT_HPP

#include <climits>
#include <cstdint>
#include <iostream>
#include <type_traits>

#include "miller_rabin.hpp"

template <unsigned int mod, std::enable_if_t<(1 <= mod), void *> = nullptr>
class modint {
  unsigned int x;

public:
  constexpr modint() : x(0) {}
  constexpr modint(const modint &v) : x(v.x) {}
  constexpr modint(modint &&v) : x(v.x) {}
  template <typename T,
            std::enable_if_t<std::is_integral_v<T>, void *> = nullptr>
  constexpr modint(T v) : x() {
    if constexpr (std::is_signed_v<T>) {
      intmax_t x_ = v % intmax_t(mod);
      if (x_ < 0)
        x_ += mod;
      x = static_cast<unsigned int>(x_);
    } else {
      x = static_cast<unsigned int>(v % mod);
    }
  }

  constexpr unsigned int val() const { return x; }

  constexpr modint pow(uint64_t k) const {
    modint r = 1, a = *this;
    while (k) {
      if (k & 1)
        r *= a;
      k >>= 1;
      a *= a;
    }
    return r;
  }

  constexpr modint inv() const {
    // TODO: support non-prime mod
    static_assert(isprime(mod));
    return pow(mod - 2);
  }
  constexpr modint operator+() const { return *this; }
  constexpr modint operator-() const {
    modint r;
    r.x = x ? mod - x : 0;
    return r;
  }

  constexpr modint &operator++() {
    x++;
    if (x == mod)
      x = 0;
    return *this;
  }
  constexpr modint &operator--() {
    if (x == 0)
      x = mod;
    x--;
    return *this;
  }
  constexpr modint operator++(int) {
    modint result = *this;
    ++*this;
    return result;
  }
  constexpr modint operator--(int) {
    modint result = *this;
    --*this;
    return result;
  }
  constexpr modint &operator=(modint v) {
    x = v.x;
    return *this;
  }
  constexpr modint &operator+=(modint v) {
    if (x < mod - v.x)
      x += v.x;
    else
      x += v.x - mod;
    return *this;
  }
  constexpr modint &operator-=(modint v) {
    if (x < v.x)
      x -= v.x - mod;
    else
      x -= v.x;
    return *this;
  }
  constexpr modint &operator*=(modint v) {
    x = static_cast<unsigned int>(uintmax_t(x) * uintmax_t(v.x) % mod);
    return *this;
  }
  constexpr modint &operator/=(modint v) {
    x = static_cast<unsigned int>(uintmax_t(x) * uintmax_t(v.inv().x) % mod);
    return *this;
  }
  friend std::istream &operator>>(std::istream &is, modint &v) {
    unsigned int y;
    is >> y;
    v.x = y % mod;
    return is;
  }
  friend constexpr std::ostream &operator<<(std::ostream &os, modint v) {
    return os << v.x;
  }
  friend constexpr modint operator+(const modint &lhs, const modint &rhs) {
    return modint(lhs) += rhs;
  }
  friend constexpr modint operator-(const modint &lhs, const modint &rhs) {
    return modint(lhs) -= rhs;
  }
  friend constexpr modint operator*(const modint &lhs, const modint &rhs) {
    return modint(lhs) *= rhs;
  }
  friend constexpr modint operator/(const modint &lhs, const modint &rhs) {
    return modint(lhs) /= rhs;
  }
  friend constexpr bool operator==(const modint &lhs, const modint &rhs) {
    return lhs.x == rhs.x;
  }
  friend constexpr bool operator!=(const modint &lhs, const modint &rhs) {
    return lhs.x != rhs.x;
  }
};

#endif // CHIKA_MODINT_HPP
