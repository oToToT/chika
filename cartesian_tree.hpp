#ifndef CHIKA_CARTESIAN_TREE_HPP
#define CHIKA_CARTESIAN_TREE_HPP

#include <ranges>
#include <span>
#include <vector>

template <typename T, typename Cmp = std::less<T>>
std::vector<int> get_cartesian_tree(std::span<T> sequence, Cmp &&cmp = Cmp{}) {
  std::vector<int> parent(sequence.size());
  std::vector<int> stk;
  stk.reserve(sequence.size());
  for (const auto &[index, element] : sequence | std::views::enumerate) {
    int last = -1;
    while (not stk.empty() and cmp(element, sequence[stk.back()])) {
      last = stk.back();
      stk.pop_back();
    }
    if (not stk.empty()) {
      parent[index] = stk.back();
    }
    if (last != -1) {
      parent[last] = static_cast<int>(index);
    }
    stk.push_back(static_cast<int>(index));
  }
  parent[stk[0]] = stk[0];
  return parent;
}

template <typename T, typename Cmp = std::less<T>>
std::vector<int> get_cartesian_tree(const std::vector<T> &sequence,
                                    Cmp &&cmp = Cmp{}) {
  return get_cartesian_tree(std::span(sequence), std::move(cmp));
}

#endif // CHIKA_CARTESIAN_TREE_HPP
