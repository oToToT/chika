#ifndef CHIKA_POLYNOMIAL_HPP
#define CHIKA_POLYNOMIAL_HPP

#include "internal_modop.hpp"
#include "modint.hpp"
#include "ntt.hpp"

#include <algorithm>
#include <utility>
#include <vector>

template <int MOD> struct Poly : public std::vector<modint<MOD>> {
  using ctnr_t = std::vector<modint<MOD>>;

  explicit Poly(int n = 0) : ctnr_t(n) {}
  Poly(const ctnr_t &v) : ctnr_t(v) {}

  Poly Mul(const Poly &rhs) const {
    if (rhs.empty() or this->empty())
      return Poly(0);
    const size_t n = n2k(this->size() + rhs.size() - 1);
    Poly X = *this, Y = rhs;
    X.resize(n);
    Y.resize(n);
    ntt(X.data(), n);
    ntt(Y.data(), n);
    for (size_t i = 0; i < n; ++i)
      X[i] *= Y[i];
    ntt(X.data(), n, true);
    X.resize(this->size() + rhs.size() - 1);
    return X;
  }
  Poly Inv() const {
    if (this->size() == 1)
      return ctnr_t{this->begin()->inv()};
    const size_t _n = n2k(this->size() * 2);
    Poly Xi = Poly(*this);
    Xi.resize((this->size() + 1) / 2);
    Xi = Xi.Inv();
    Xi.resize(_n);
    Poly Y(*this);
    Y.resize(_n);
    ntt(Xi.data(), _n);
    ntt(Y.data(), _n);
    for (size_t i = 0; i < _n; ++i)
      Xi[i] *= 2 - (Xi[i] * Y[i]);
    ntt(Xi.data(), _n, true);
    Xi.resize(this->size());
    return Xi;
  }
  std::pair<Poly, Poly> DivMod(const Poly &rhs) const {
    if (this->size() < rhs.size())
      return {Poly(ctnr_t{0}).trim(), Poly(*this).trim()};
    const size_t _n = this->size() - rhs.size() + 1;
    Poly X(rhs);
    std::reverse(X.begin(), X.end());
    X.resize(_n);
    Poly Y(*this);
    std::reverse(Y.begin(), Y.end());
    Y.resize(_n);
    Poly Q = Y.Mul(X.Inv());
    Q.resize(_n);
    std::reverse(Q.begin(), Q.end());
    X = rhs.Mul(Q), Y = *this;
    for (size_t i = 0; i < this->size(); ++i)
      Y[i] -= X[i];
    Y.resize(rhs.size() - 1);
    return {Q.trim(), Y.trim()};
  }
  Poly Dx() const {
    if (this->empty())
      return Poly(0);
    Poly ret(this->size() - 1);
    for (size_t i = 0; i < ret.size(); ++i)
      ret[i] = (i + 1) * this->data()[i + 1];
    return ret;
  }
  Poly Sx() const {
    Poly ret(this->size() + 1);
    for (size_t i = 0; i < this->size(); ++i)
      ret[i + 1] = this->data()[i] / (i + 1);
    return ret;
  }
  Poly Ln() const {
    Poly r = this->Dx().Mul(Inv()).Sx();
    r.resize(this->size());
    return r;
  }
  Poly Exp() const {
    if (this->size() == 1)
      return ctnr_t{1};
    Poly X(*this);
    X.resize((this->size() + 1) / 2);
    X = X.Exp();
    X.resize(this->size());
    Poly Y = X.Ln();
    Y[0] = -1;
    for (size_t i = 0; i < this->size(); ++i)
      Y[i] = this->data()[i] - Y[i];
    X = X.Mul(Y);
    X.resize(this->size());
    return X;
  }
  ctnr_t Eval(const ctnr_t &x) const {
    if (x.empty())
      return {};

    Poly P(*this);
    const size_t n = std::max(x.size(), P.size());

    std::vector<Poly> t(n * 2, ctnr_t{1, 0}), f(n * 2);
    for (size_t i = 0; i < x.size(); ++i)
      t[n + i] = ctnr_t{1, -x[i]};
    for (size_t i = n - 1; i > 0; --i)
      t[i] = t[i * 2].Mul(t[i * 2 + 1]);
    ctnr_t res(x.size());
    P.resize(n);
    std::reverse(P.begin(), P.end());
    f[1] = P.Mul(t[1].Inv());
    f[1].resize(n);
    std::reverse(f[1].begin(), f[1].end());
    for (size_t i = 1; i < n; ++i) {
      auto tmp = f[i];
      std::reverse(tmp.begin(), tmp.end());
      f[i * 2] = tmp.Mul(t[i * 2 + 1]), f[i * 2 + 1] = tmp.Mul(t[i * 2]);
      f[i * 2].resize(tmp.size()), f[i * 2 + 1].resize(tmp.size());
      std::reverse(f[i * 2].begin(), f[i * 2].end());
      std::reverse(f[i * 2 + 1].begin(), f[i * 2 + 1].end());
      f[i * 2].resize(t[i * 2].size());
      f[i * 2 + 1].resize(t[i * 2 + 1].size());
    }
    for (size_t i = 0; i < x.size(); ++i)
      res[i] = f[n + i][0];
    return res;
  }
  Poly trim() const {
    Poly P(*this);
    while (not P.empty() and P.back() == 0)
      P.pop_back();
    return P;
  }

  static Poly Interpolate(const ctnr_t &x, const ctnr_t &y) {
    const int _n = int(x.size());
    std::vector<Poly> up = _tree1(x), down(_n * 2);
    ctnr_t z = up[1].Dx()._eval(x, up);
    for (int i = 0; i < _n; ++i)
      z[i] = y[i] / z[i];
    for (int i = 0; i < _n; ++i)
      down[_n + i] = ctnr_t{z[i]};
    for (int i = _n - 1; i > 0; --i) {
      down[i] = down[i * 2].Mul(up[i * 2 + 1]);
      auto rhs = down[i * 2 + 1].Mul(up[i * 2]);
      for (size_t j = 0; j < std::min(down[i].size(), rhs.size()); ++j)
        down[i][j] += rhs[j];
    }
    return down[1];
  }

private:
  template <typename... Args> static void ntt(Args &&...args) {
    static NTT<MOD> ntt_;
    return ntt_(args...);
  }
  ctnr_t _eval(const ctnr_t &x, const std::vector<Poly> &up) const {
    if (x.empty())
      return {};
    std::vector<Poly> down(x.size() * 2);
    down[1] = DivMod(up[1]).second;
    for (size_t i = 2; i < x.size() * 2; ++i)
      down[i] = down[i / 2].DivMod(up[i]).second;
    ctnr_t y(x.size());
    for (size_t i = 0; i < x.size(); ++i) {
      const auto &d = down[x.size() + i];
      y[i] = d.empty() ? 0 : d[0];
    }
    return y;
  }
  static uint64_t n2k(uint64_t n) {
    if (n <= 1)
      return 1;
    return 1ull << (64 - __builtin_clzll(n - 1));
  }
  static uint32_t n2k(uint32_t n) {
    if (n <= 1)
      return 1;
    return 1u << (32 - __builtin_clz(n - 1));
  }
  static std::vector<Poly> _tree1(const ctnr_t &x) {
    std::vector<Poly> up(x.size() * 2);
    for (size_t i = 0; i < x.size(); ++i)
      up[x.size() + i] = ctnr_t{-x[i], 1};
    for (size_t i = 1; i < x.size(); ++i)
      up[x.size() - i] = up[(x.size() - i) * 2].Mul(up[(x.size() - i) * 2 + 1]);
    return up;
  }
};

#endif // CHIKA_POLYNOMIAL_HPP
