#ifndef CHIKA_FACTORIZE_HPP
#define CHIKA_FACTORIZE_HPP

#include <array>
#include <numeric>
#include <random>
#include <utility>
#include <vector>

#include "internal_modop.hpp"
#include "miller_rabin.hpp"
#include "prng.hpp"

template <typename T> constexpr T getFactor(T n) {
  auto f = [](T x, T k, T m) constexpr {
    return Chika::madd(k, Chika::mmul(x, x, m), m);
  };
  if (n % 2 == 0)
    return 2;

  uint64_t rng[2] = {Chika::PRNG_DEFAULT[0], Chika::PRNG_DEFAULT[1]};

  while (true) {
    T y = 2, yy = y, x = xorshift128plus(rng) % n, t = 1;
    for (T sz = 2; t == 1; sz <<= 1) {
      for (T i = 0; i < sz; ++i) {
        if (t != 1)
          break;
        yy = f(yy, x, n);
        t = std::gcd(yy > y ? yy - y : y - yy, n);
      }
      y = yy;
    }
    if (t != 1 && t != n)
      return t;
  }
}

template <typename T> std::vector<T> factorize(T x) {
  if (x == 1)
    return {};
  std::vector<T> r = {x};
  for (size_t i = 0; i < r.size(); ++i) {
    if (isprime(r[i]))
      continue;
    T f = getFactor(r[i]);
    r.push_back(r[i] / f);
    r[i--] = f;
  }
  return r;
}

template <typename T, size_t N,
          std::enable_if_t<N >= 1, void *> = nullptr>
constexpr std::pair<std::array<T, N>, size_t> factorize(T x) {
  if (x == 1)
    return std::pair{std::array<T, N>{}, 0};
  std::array<T, N> r = {x};
  size_t Size = 1;
  for (size_t i = 0; i < Size; ++i) {
    if (isprime(r[i]))
      continue;
    T f = getFactor(r[i]);
    r[Size++] = r[i] / f;
    r[i--] = f;
  }
  return {std::move(r), Size};
}

#endif // CHIKA_FACTORIZE_HPP
