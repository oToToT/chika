#ifndef CHIKA_NTT_HPP
#define CHIKA_NTT_HPP

#include "internal_modop.hpp"
#include "miller_rabin.hpp"
#include "modint.hpp"
#include "primitive_root.hpp"

#include <cstddef>
#include <type_traits>

template <uint64_t MOD, std::enable_if_t<isprime(MOD), void *> = nullptr>
class NTT {
private:
  static constexpr size_t calcMaxn() {
    size_t r = 1;
    for (size_t x = MOD - 1; x % 2 == 0; x /= 2)
      r *= 2;
    return r;
  }

public:
  static constexpr size_t maxn = calcMaxn();
  constexpr NTT() {
    modint<MOD> r =
        Chika::mpow(PrimePrimitiveRoot<MOD>::value, (MOD - 1) / maxn, MOD);
    for (int i = maxn >> 1; i; i >>= 1) {
      roots[i] = 1;
      for (int j = 1; j < i; j++)
        roots[i + j] = roots[i + j - 1] * r;
      r *= r;
    }
  }
  // n must be 2^k
  constexpr void operator()(modint<MOD> F[], size_t n, bool inv = false) const {
    for (size_t i = 0, j = 0; i < n; i++) {
      if (i < j) {
        auto tmp = F[i];
        F[i] = F[j];
        F[j] = tmp;
      }
      for (size_t k = n >> 1; (j ^= k) < k; k >>= 1)
        ;
    }
    for (size_t s = 1; s < n; s *= 2) {
      for (size_t i = 0; i < n; i += s * 2) {
        for (size_t j = 0; j < s; j++) {
          auto a = F[i + j];
          auto b = F[i + j + s] * roots[s + j];
          F[i + j] = a + b;
          F[i + j + s] = a - b;
        }
      }
    }
    if (inv) {
      const modint<MOD> invn = modint<MOD>(n).inv();
      for (size_t i = 0; i < n; i++)
        F[i] *= invn;
      for (size_t i = 1; i + i + 1 < n; ++i) {
        auto tmp = F[i];
        F[i] = F[n - i];
        F[n - i] = tmp;
      }
    }
  }

private:
  modint<MOD> roots[maxn];
};

#endif // CHIKA_NTT_HPP
