#ifndef CHIKA_PRIMES_HPP
#define CHIKA_PRIMES_HPP

#include <cinttypes>
#include <vector>

class Primes {
  std::vector<bool> sieved;
  std::vector<int> primes;

public:
  Primes(int N) : sieved(N) {
    sieved[0] = true;
    if (N > 1)
      sieved[1] = true;
    for (int i = 2; i < N; ++i) {
      if (not sieved[i]) {
        primes.push_back(i);
      }
      for (int p : primes) {
        if (int64_t(i) * p >= N)
          break;
        sieved[i * p] = true;
        if (i % p == 0)
          break;
      }
    }
  }
  auto rbegin() const { return primes.rbegin(); }
  auto rend() const { return primes.rend(); }
  auto begin() const { return primes.begin(); }
  auto end() const { return primes.end(); }
  auto cbegin() const { return primes.cbegin(); }
  auto cend() const { return primes.cend(); }
  int bound() const { return int(sieved.size()); }
  int count() const { return int(primes.size()); }
  bool isprime(int x) const { return not sieved[x]; }
  int operator[](int x) const { return primes[x]; }
};

#endif // CHIKA_PRIMES_HPP
