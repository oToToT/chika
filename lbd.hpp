#ifndef CHIKA_LBD_HPP
#define CHIKA_LBD_HPP

#include <algorithm>
#include <utility>
#include <vector>

class LBD {
private:
  static int lowbit(int x) { return x & (-x); }
  int timer, chains;
  std::vector<std::vector<int>> G;
  std::vector<int> tl, tr, chain, chain_st, dep, pa;
  // chain_ : number of chain
  // tl, tr[ u ] : subtree interval in the seq. of u
  // chain_st[ u ] : head of the chain contains u
  // chian[ u ] : chain id of the chain u is on
  void predfs(int u, int f) {
    dep[u] = dep[pa[u] = f] + 1;
    for (int v : G[u]) {
      if (v == f)
        continue;
      predfs(v, u);
      if (lowbit(chain[u]) < lowbit(chain[v]))
        chain[u] = chain[v];
    }
    if (chain[u] == 0)
      chain[u] = ++chains;
  }
  void dfschain(int u, int f) {
    tl[u] = timer++;
    if (chain_st[chain[u]] == -1)
      chain_st[chain[u]] = u;
    for (int v : G[u])
      if (v != f and chain[v] == chain[u])
        dfschain(v, u);
    for (int v : G[u])
      if (v != f and chain[v] != chain[u])
        dfschain(v, u);
    tr[u] = timer;
  }

public:
  LBD(int n)
      : timer(0), chains(0), G(n), tl(n), tr(n), chain(n), chain_st(n, -1),
        dep(n), pa(n) {}
  void add_edge(int u, int v) {
    G[u].push_back(v);
    G[v].push_back(u);
  }
  void decompose(int r = 0) {
    predfs(r, r);
    dfschain(r, r);
  }
  int lca(int u, int v) const {
    while (chain[u] != chain[v]) {
      if (dep[chain_st[chain[u]]] < dep[chain_st[chain[v]]])
        std::swap(u, v);
      u = pa[chain_st[chain[u]]];
    }
    if (dep[u] < dep[v])
      return u;
    return v;
  }
  int dis(int u, int v) const { return dep[u] + dep[v] - 2 * dep[lca(u, v)]; }
  std::pair<int, int> get_subtree(int u) const { return {tl[u], tr[u]}; }
  std::vector<std::pair<int, int>> get_path(int u, int v) const {
    std::vector<std::pair<int, int>> res;
    while (chain[u] != chain[v]) {
      if (dep[chain_st[chain[u]]] < dep[chain_st[chain[v]]])
        std::swap(u, v);
      int s = chain_st[chain[u]];
      res.emplace_back(tl[s], tl[u] + 1);
      u = pa[s];
    }
    if (dep[u] < dep[v])
      std::swap(u, v);
    res.emplace_back(tl[v], tl[u] + 1);
    return res;
  }
};

#endif // CHIKA_LBD_HPP
