#ifndef CHIKA_INTERNAL_MODOP_HPP
#define CHIKA_INTERNAL_MODOP_HPP

#include <cinttypes>

namespace Chika {

template <typename T> constexpr auto madd(T a, T b, T n) {
  if (a >= n - b)
    return a + b - n;
  return a + b;
}

constexpr auto mmul(uint64_t a, uint64_t b, uint64_t n) {
#ifdef __SIZEOF_INT128__
  return static_cast<uint64_t>(__uint128_t(a) * b % n);
#else

  auto add_n = [n](uint64_t x, uint64_t y) {
    if (x >= n - y)
      return x + y - n;
    return x + y;
  };

  const uint64_t low_a = a >> 32;
  const uint64_t up_a = a & uint32_t(-1);
  const uint64_t low_b = b >> 32;
  const uint64_t up_b = b & uint32_t(-1);

  const uint64_t lo = low_a * low_b % n;
  const uint64_t mi = (add_n(low_a * up_b % n, up_a * low_b % n) << 32) % n;
  const uint64_t hi =
      (up_a * up_b % n) * add_n((1u << 63) % n, (1u << 63) % n) % n;

  return add_n(lo, add_n(mi, hi));
#endif
}

constexpr auto mmul(uint32_t a, uint32_t b, uint32_t n) {
  return static_cast<uint32_t>(uint64_t(a) * uint64_t(b) % n);
}

template <typename T> constexpr auto mpow(T a, uint64_t b, T n) {
  T r = 1;
  while (b) {
    if (b & 1)
      r = mmul(r, a, n);
    b >>= 1;
    a = mmul(a, a, n);
  }
  return r % n;
}
}; // namespace Chika

#endif // CHIKA_INTERNAL_MODOP_HPP
