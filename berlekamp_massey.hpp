#ifndef CHIKA_BERLEKAMP_MASSEY_HPP
#define CHIKA_BERLEKAMP_MASSEY_HPP

#include "eps.hpp"

#include <cmath>
#include <type_traits>
#include <vector>

template <typename T>
std::vector<T> BerlekampMassey(const std::vector<T> &output) {
  std::vector<T> d(output.size() + 1), me, he;
  size_t fhe = 0, fme = 0;
  bool first = true;
  for (size_t i = 1; i <= output.size(); ++i) {
    for (size_t j = 0; j < me.size(); ++j) {
      d[i] += output[i - j - 2] * me[j];
    }
    d[i] -= output[i - 1];
    if constexpr (std::is_floating_point_v<T>) {
      if (std::abs(d[i]) < EPS)
        continue;
    } else {
      if (d[i] == T(0))
        continue;
    }
    fme = i;
    if (first) {
      first = false;
      fhe = fme;
      me.resize(i);
      continue;
    }
    std::vector<T> o(i - fhe - 1);
    T k = -d[i] / d[fhe];
    o.push_back(-k);
    for (T x : he) {
      o.push_back(x * k);
    }
    if (o.size() < me.size()) {
      o.resize(me.size());
    }
    for (size_t j = 0; j < me.size(); ++j) {
      o[j] += me[j];
    }
    if (i - fhe + he.size() >= me.size()) {
      he = me;
      fhe = fme;
    }
    me = o;
  }
  return me;
}

#endif // CHIKA_BERLEKAMP_MASSEY_HPP
