#ifndef CHIKA_SHORTEST_PATH_HPP
#define CHIKA_SHORTEST_PATH_HPP

#include <limits>
#include <queue>
#include <utility>
#include <vector>

// Time complexity: O(V + E log V)
template <typename T, bool return_path = false>
auto shortest_path(const std::vector<std::vector<std::pair<int, T>>> &graph,
                   int s) {
  std::vector<int> from(graph.size());
  std::vector<T> d(graph.size(), std::numeric_limits<T>::max());
  std::priority_queue<std::pair<T, int>, std::vector<std::pair<T, int>>,
                      std::greater<>>
      q;
  q.emplace(d[s] = 0, s);
  while (not q.empty()) {
    auto [l, u] = q.top();
    q.pop();
    if (l != d[u])
      continue;
    for (auto [v, w] : graph[u]) {
      if (l + w < d[v]) {
        q.emplace(d[v] = l + w, v);
        if constexpr (return_path) {
          from[v] = u;
        }
      }
    }
  }
  if constexpr (return_path)
    return std::pair{d, from};
  else
    return d;
}

// Time complexity: O(V + E)
template <bool return_path = false>
auto shortest_path(const std::vector<std::vector<int>> &graph, int s) {
  std::vector<int> from(graph.size());
  std::vector<int> d(graph.size(), std::numeric_limits<int>::max());
  std::queue<int> q;
  q.push(s);
  d[s] = 0;
  while (not q.empty()) {
    auto u = q.front();
    q.pop();
    for (auto v : graph[u]) {
      if (d[u] + 1 < d[v]) {
        q.emplace(v);
        d[v] = d[u] + 1;
        if constexpr (return_path) {
          from[v] = u;
        }
      }
    }
  }
  if constexpr (return_path)
    return std::pair{d, from};
  else
    return d;
}

// Time complexity: O(V^2)
template <typename T, bool return_path = false>
auto shortest_path_dense(
    const std::vector<std::vector<std::pair<int, T>>> &graph, int s) {
  std::vector<int> from(graph.size());
  std::vector<bool> done(graph.size());
  std::vector<T> d(graph.size(), std::numeric_limits<T>::max());
  d[s] = 0;
  for (std::size_t iter = 0; iter < graph.size(); ++iter) {
    int u = -1;
    for (std::size_t i = 0; i < graph.size(); ++i) {
      if (done[i] or d[i] == std::numeric_limits<T>::max()) {
        continue;
      }
      if (u == -1 or d[i] < d[u]) {
        u = int(i);
      }
    }
    if (u == -1) {
      break;
    }
    done[u] = true;
    for (auto [v, w] : graph[u]) {
      if (d[u] + w < d[v]) {
        d[v] = d[u] + w;
        if constexpr (return_path) {
          from[v] = u;
        }
      }
    }
  }
  if constexpr (return_path)
    return std::pair{d, from};
  else
    return d;
}

#endif // CHIKA_SHORTEST_PATH_HPP
