#ifndef CHIKA_BCC_HPP
#define CHIKA_BCC_HPP

#include <algorithm>
#include <utility>
#include <vector>

class BCC {
  int n, ecnt, bcnt;
  std::vector<std::vector<std::pair<int, int>>> g;
  std::vector<int> dfn, low, bcc, stk;
  std::vector<bool> ap, bridge;
  void dfs(int u, int f) {
    dfn[u] = low[u] = dfn[f] + 1;
    int ch = 0;
    for (auto [v, t] : g[u])
      if (bcc[t] == -1) {
        bcc[t] = 0;
        stk.push_back(t);
        if (dfn[v]) {
          low[u] = std::min(low[u], dfn[v]);
        } else {
          ++ch, dfs(v, u);
          low[u] = std::min(low[u], low[v]);
          if (low[v] > dfn[u])
            bridge[t] = true;
          if (low[v] >= dfn[u]) {
            ap[u] = true;
            while (not stk.empty()) {
              int o = stk.back();
              bcc[o] = bcnt;
              stk.pop_back();
              if (o == t)
                break;
            }
            bcnt += 1;
          }
        }
      }
    ap[u] = ap[u] and (ch != 1 or u != f);
  }

public:
  BCC(int n_) : n(n_), ecnt(0), bcnt(0), g(n), dfn(n), low(n), stk(), ap(n) {}
  void add_edge(int u, int v) {
    g[u].emplace_back(v, ecnt);
    g[v].emplace_back(u, ecnt++);
  }
  void solve() {
    bridge.assign(ecnt, false);
    bcc.assign(ecnt, -1);
    for (int i = 0; i < n; ++i)
      if (not dfn[i])
        dfs(i, i);
  }
  int bcc_id(int x) const { return bcc[x]; }
  bool is_ap(int x) const { return ap[x]; }
  bool is_bridge(int x) const { return bridge[x]; }
};

#endif
