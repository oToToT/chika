#ifndef CHIKA_GCD_CONVOLUTION_HPP
#define CHIKA_GCD_CONVOLUTION_HPP

#include <cassert>
#include <vector>

#include "primes.hpp"

template <typename T>
std::vector<T> gcd_convolution(std::vector<T> a, std::vector<T> b) {
  assert(a.size() == b.size());
  Primes primes(a.size());
  const int n = int(a.size()) - 1;
  auto zeta = [&](std::vector<T> &v) {
    for (auto p : primes) {
      for (int k = n / p; k >= 1; k--) {
        v[k] += v[k * p];
      }
    }
  };
  auto mobius = [&](std::vector<T> &v) {
    for (auto p : primes) {
      for (int64_t k = 1; k * p <= n; k++) {
        v[k] -= v[k * p];
      }
    }
  };
  zeta(a), zeta(b);
  for (int i = 0; i <= n; ++i)
    a[i] *= b[i];
  mobius(a);
  return a;
}

template <typename T>
std::vector<T> gcd_convolution(std::vector<T> a, std::vector<T> b,
                               const Primes &primes) {
  assert(a.size() == b.size() and int(a.size()) == primes.bound());
  const int n = int(a.size()) - 1;
  auto zeta = [&](std::vector<T> &v) {
    for (auto p : primes) {
      for (int k = n / p; k >= 1; k--) {
        v[k] += v[k * p];
      }
    }
  };
  auto mobius = [&](std::vector<T> &v) {
    for (auto p : primes) {
      for (int64_t k = 1; k * p <= n; k++) {
        v[k] -= v[k * p];
      }
    }
  };
  zeta(a), zeta(b);
  for (int i = 0; i <= n; ++i)
    a[i] *= b[i];
  mobius(a);
  return a;
}

#endif // CHIKA_GCD_CONVOLUTINO_HPP
