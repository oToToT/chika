#ifndef CHIKA_SAIS_HPP
#define CHIKA_SAIS_HPP

#include <cstring>
#include <string>
#include <type_traits>

// sa[i]: sa[i]-th suffix is the i-th lexigraphically smallest suffix.
// hi[i]: longest common prefix of suffix sa[i] and suffix sa[i - 1].

template <int maxn, typename T = char, T maxc = 127> class SAIS {
  using V = std::common_type_t<T, int>;
  bool _t[maxn * 2];
  V _s[maxn * 2], _c[maxn * 2];
  int _q[maxn * 2], _p[maxn];
  V x[maxn];
  void pre(int *S, V *c, int n, V z) {
    memset(S, 0, sizeof(int) * n);
    memcpy(x, c, sizeof(V) * z);
  }
  void induce(int *a, V *c, V *s, bool *t, int n, V z) {
    memcpy(x + 1, c, sizeof(V) * (z - 1));
    for (int i = 0; i < n; ++i)
      if (a[i] && !t[a[i] - 1])
        a[x[s[a[i] - 1]]++] = a[i] - 1;
    memcpy(x, c, sizeof(V) * z);
    for (int i = n - 1; i >= 0; --i)
      if (a[i] && t[a[i] - 1])
        a[--x[s[a[i] - 1]]] = a[i] - 1;
  }
  void sais(V *s, int *a, int *p, int *q, bool *t, V *c, int n, V z) {
    bool uniq = t[n - 1] = true;
    int nn = 0, nmxz = -1, *nsa = a + n, last = -1;
    V *ns = s + n;
    memset(c, 0, sizeof(V) * z);
    for (int i = 0; i < n; ++i)
      uniq &= ++c[s[i]] < 2;
    for (int i = 0; i < z - 1; ++i)
      c[i + 1] += c[i];
    if (uniq) {
      for (int i = 0; i < n; ++i)
        a[--c[s[i]]] = i;
      return;
    }
    for (int i = n - 2; i >= 0; --i)
      t[i] = (s[i] == s[i + 1] ? t[i + 1] : s[i] < s[i + 1]);
    pre(a, c, n, z);
    for (int i = 1; i <= n - 1; ++i)
      if (t[i] && !t[i - 1])
        a[--x[s[i]]] = p[q[i] = nn++] = i;
    induce(a, c, s, t, n, z);
    for (int i = 0; i < n; ++i) {
      if (a[i] && t[a[i]] && !t[a[i] - 1]) {
        bool neq = last < 0 || memcmp(s + a[i], s + last,
                                      (p[q[a[i]] + 1] - a[i]) * sizeof(V));
        ns[q[last = a[i]]] = nmxz += neq;
      }
    }
    sais(ns, nsa, p + nn, q + n, t + n, c + z, nn, nmxz + 1);
    pre(a, c, n, z);
    for (int i = nn - 1; i >= 0; --i)
      a[--x[s[p[nsa[i]]]]] = p[nsa[i]];
    induce(a, c, s, t, n, z);
  }

public:
  int sa[maxn * 2], hi[maxn], rev[maxn];
  void build(const std::basic_string<T> &s) {
    const auto n = int(s.size());
    for (int i = 0; i < n; ++i)
      _s[i] = s[i];
    _s[n] = 0; // s sohuldn't contain 0
    sais(_s, sa, _p, _q, _t, _c, n + 1, maxc);
    for (int i = 0; i < n; ++i)
      rev[sa[i] = sa[i + 1]] = i;
    int ind = hi[0] = 0;
    for (int i = 0; i < n; ++i) {
      if (!rev[i]) {
        ind = 0;
        continue;
      }
      while (i + ind < n && s[i + ind] == s[sa[rev[i] - 1] + ind])
        ++ind;
      hi[rev[i]] = ind ? ind-- : 0;
    }
  }
};

#endif // CHIKA_SAIS_HPP
