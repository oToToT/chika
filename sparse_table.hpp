#ifndef CHIKA_SPARSE_TABLE
#define CHIKA_SPARSE_TABLE

#include <functional>
#include <vector>

template <typename T, typename Cmp_ = std::less<T>> class SparseTable {
private:
  std::vector<std::vector<T>> table;
  std::vector<int> lg;
  T op(T a, T b, Cmp_ cmp = {}) const { return cmp(a, b) ? a : b; }

public:
  SparseTable(const T a[], int n) : lg(n + 1, -1) {
    // 0-base
    for (int i = 1; i <= n; ++i)
      lg[i] = lg[i >> 1] + 1;
    table.resize(lg[n] + 1);
    table[0] = std::vector<T>(a, a + n);
    for (std::size_t i = 1; i < table.size(); ++i) {
      const int len = 1 << (i - 1), sz = 1 << i;
      table[i].resize(n - sz + 1);
      for (int j = 0; j <= n - sz; ++j)
        table[i][j] = op(table[i - 1][j], table[i - 1][j + len]);
    }
  }
  explicit SparseTable(const std::vector<T> &a) : SparseTable(a.data(), int(a.size())) {}
  T query(int l, int r) const {
    // 0-base [l, r)
    const int wh = lg[r - l], len = 1 << wh;
    return op(table[wh][l], table[wh][r - len]);
  }
};

#endif // CHIKA_SPARSE_TABLE
