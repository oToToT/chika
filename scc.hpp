#ifndef CHIKA_SCC_HPP
#define CHIKA_SCC_HPP

#include <algorithm>
#include <vector>

class SCC {
private:
  int n, num_;
  std::vector<std::vector<int>> G, rG;
  std::vector<int> ord, num;
  std::vector<bool> vis;
  void dfs(int u) {
    if (vis[u])
      return;
    vis[u] = true;
    for (int v : G[u])
      dfs(v);
    ord.push_back(u);
  }
  void rdfs(int u) {
    if (not vis[u])
      return;
    num[u] = num_;
    vis[u] = false;
    for (int v : rG[u])
      rdfs(v);
  }

public:
  SCC(int n_) : n(n_), num_(0), G(n), rG(n), num(n), vis(n) {}
  void add_edge(int st, int ed) {
    G[st].push_back(ed);
    rG[ed].push_back(st);
  }
  void solve() {
    for (int i = 0; i < n; ++i)
      if (not vis[i])
        dfs(i);
    std::reverse(ord.begin(), ord.end());
    for (int i : ord) {
      if (vis[i]) {
        rdfs(i);
        num_++;
      }
    }
  }
  int get(int x) const { return num[x]; }
  int count() const { return num_; }
};

#endif // CHIKA_SCC_HPP
