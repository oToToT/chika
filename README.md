Chika
===

A competitive programming library in C++23.

Code verified with [@yosupo06/library-checker-problems](https://judge.yosupo.jp/) is under the `tests` folder.

You can use [@oToToT/CppBundler](https://github.com/oToToT/CppBundler) to bundle your code with this library.
