#ifndef CHIKA_DINIC_HPP
#define CHIKA_DINIC_HPP

#include <cstdint>
#include <limits>
#include <queue>
#include <utility>
#include <vector>

template <typename Cap = int64_t> class Dinic {
private:
  struct E {
    int to, rev;
    Cap cap;
  };
  int n, ed;
  std::vector<std::vector<E>> G;
  std::vector<int> lv, idx;
  std::vector<std::pair<int, int>> inv;
  bool BFS(int st) {
    lv.assign(n, -1);
    std::queue<int> bfs;
    bfs.push(st);
    lv[st] = 0;
    while (not bfs.empty()) {
      int u = bfs.front();
      bfs.pop();
      for (auto e : G[u]) {
        if (e.cap <= 0 or lv[e.to] != -1)
          continue;
        bfs.push(e.to);
        lv[e.to] = lv[u] + 1;
      }
    }
    return lv[ed] != -1;
  }
  Cap DFS(int u, Cap f) {
    if (u == ed)
      return f;
    Cap ret = 0;
    for (int &i = idx[u]; i < int(G[u].size()); ++i) {
      auto &e = G[u][i];
      if (e.cap <= 0 or lv[e.to] != lv[u] + 1)
        continue;
      Cap nf = DFS(e.to, std::min(f, e.cap));
      ret += nf;
      e.cap -= nf;
      f -= nf;
      G[e.to][e.rev].cap += nf;
      if (f == 0)
        return ret;
    }
    if (ret == 0)
      lv[u] = -1;
    return ret;
  }

public:
  Dinic(int n_) : n(n_), G(n) {}
  void add_edge(int u, int v, Cap c) {
    G[u].push_back({v, int(G[v].size()), c});
    G[v].push_back({u, int(G[u].size()) - 1, 0});
    inv.emplace_back(v, int(G[v].size()) - 1);
  }
  Cap operator()(int st, int ed_) {
    ed = ed_;
    Cap ret = 0;
    while (BFS(st)) {
      idx.assign(n, 0);
      Cap f = DFS(st, std::numeric_limits<Cap>::max());
      ret += f;
      if (f == 0)
        break;
    }
    return ret;
  }
  Cap operator[](int i) const { return G[inv[i].first][inv[i].second].cap; }
  std::vector<bool> get_visible(int s) const {
    std::vector<bool> vis(n);
    std::queue<int> bfs;
    bfs.push(s);
    vis[s] = true;
    while (not bfs.empty()) {
      int u = bfs.front();
      bfs.pop();
      for (auto e : G[u]) {
        if (vis[e.to] or e.cap == 0)
          continue;
        bfs.push(e.to);
        vis[e.to] = true;
      }
    }
    return vis;
  }
};

#endif // CHIKA_DINIC_HPP
