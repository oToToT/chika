#ifndef CHIKA_ZALGORITHM_HPP
#define CHIKA_ZALGORITHM_HPP

#include <algorithm>
#include <string>
#include <vector>

template <typename T> std::vector<int> zalgo(const std::basic_string<T> &s) {
  if (s.empty()) {
    return {};
  }
  std::vector<int> z(s.size(), s.size());
  for (int i = 1, l = 0, r = 0; i < z[0]; ++i) {
    int j = std::clamp(r - i, 0, z[i - l]);
    while (i + j < z[0] and s[i + j] == s[j])
      ++j;
    if (i + (z[i] = j) > r)
      r = i + z[l = i];
  }
  return z;
}

#endif // CHIKA_ZALGORITHM
